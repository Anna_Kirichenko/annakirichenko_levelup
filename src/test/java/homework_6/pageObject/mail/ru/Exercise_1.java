package homework_6.pageObject.mail.ru;

import homewrok_6.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class Exercise_1 extends BaseTest {

    @Test
    public void MailTest() {

        // 1. Login
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.login(MAIL_LOGIN, MAIL_PASSWORD);

        // 2. verify success entrance
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.validatePage();

        // 3. create new email
        inboxPage.pressCreateEmailButton();
        EmailPage emailPage = new EmailPage(driver);
        emailPage.fillToField(MAIL_SEND_ADDRESS);
        emailPage.fillSubjectField(MAIL_SUBJECT);
        emailPage.fillEmailText(MAIL_TEXT);

        // 4. Save draft of email
        emailPage.pressSaveButton();
        emailPage.pressCloseButton();

        // 5. verify email in drafts
        DraftPage draftPage = new DraftPage(driver);
        draftPage.draftClick();
        draftPage.findEmailInDraft(0);

        // 6. verify content,address and topic of email
        assertEquals(MAIL_SEND_ADDRESS, draftPage.getMailSendAddress(0));
        assertEquals(MAIL_SUBJECT, draftPage.getMailSubject(0));
        assertEquals(MAIL_TEXT, draftPage.getMailText(0));

        // 7. Send draft
        draftPage.openEmailbyIndex(0);
        emailPage.pressSendButton();
        emailPage.pressCloseButton();

        // 8. Verify that draft box is empty
        assertEquals("У вас нет незаконченных\nили неотправленных писем", draftPage.checkEmptyfolder());

        // 9. Verify email in send folder
        SendMailPage sendMailpage = new SendMailPage(driver);
        sendMailpage.navigatetoSendEmails();
        sendMailpage.findEmailInSendMails(0);

        // 10. Exit account
        homePage.exitAccount();

    }


}
