package homework_6.pageObject.mail.ru;

import homewrok_6.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class Exercise_2 extends BaseTest {


    @Test
    public void MailTest() {

        // 1. Login
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.login(MAIL_LOGIN, MAIL_PASSWORD);

        // 2. verify success entrance
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.validatePage();

        // 3. create new
        inboxPage.pressCreateEmailButton();
        EmailPage emailPage = new EmailPage(driver);
        emailPage.fillToField(MAIL_SEND_ADDRESS);
        emailPage.fillSubjectField(MAIL_TESTSUBJECT);
        emailPage.fillEmailText(MAIL_TESTSUBJECT);

        // 4. Send email
        emailPage.pressSendButton();
        emailPage.pressCloseButton();

        // 5. Verify email in send folder
        SendMailPage sendMailPage = new SendMailPage(driver);
        sendMailPage.navigatetoSendEmails();
        sendMailPage.findEmailInSendMails(0);

        // 6. Verify email in Test folder
        TestFolderPage testFolderPage = new TestFolderPage(driver);
        testFolderPage.testFolderClick();
        testFolderPage.findEmailInTestFolder(0);

        // 7. verify content,address and topic of email
        assertEquals(MAIL_SEND_NAME, testFolderPage.getMailSendAddress(0));
        assertEquals(MAIL_TESTSUBJECT, testFolderPage.getMailSubject(0));
        assertEquals(MAIL_TESTSUBJECT, testFolderPage.getMailText(0));

        // 8. Exit account
        homePage.exitAccount();

    }


}
