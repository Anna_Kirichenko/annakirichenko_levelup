package homework_6.pageObject.mail.ru;

import homewrok_6.*;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PageObjectEx1 extends BaseTest {
    @Test
    public void MailTest() {

        // 1. Login
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.login(MAIL_LOGIN, MAIL_PASSWORD);

        // 2. verify success entrance
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.validatePage();

        // 5. Verify email in inbox folder
        inboxPage.inboxClick();
        inboxPage.clickSelfMails();
        inboxPage.findEmailInInbox(0);

        assertEquals(MAIL_SEND_NAME, inboxPage.getMailSendAddress(0));
        assertEquals(MAIL_SEND_NAME, inboxPage.getMailSubject(0));
        assertEquals(MAIL_SEND_NAME, inboxPage.getMailText(0));

    }
}

