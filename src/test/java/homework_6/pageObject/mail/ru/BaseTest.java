package homework_6.pageObject.mail.ru;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public abstract class BaseTest {

    protected WebDriver driver;
    protected WebDriverWait wait;
    public final String MAIL_URL = "https://mail.ru";
    public final String MAIL_LOGIN = "anna_kirichenko_levelup@mail.ru";
    public final String MAIL_PASSWORD = "Ktnj2020";
    public final String MAIL_SEND_ADDRESS = "anna_kirichenko_levelup@mail.ru";
    public final String MAIL_SEND_NAME = "Anna Kirichenko";
    public final String MAIL_SUBJECT = "First email of John Doe";
    public final String MAIL_TEXT = "Hi there. How are you?";
    public final String MAIL_TESTSUBJECT = "Test";

    @BeforeMethod
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(MAIL_URL);
        wait = new WebDriverWait(driver, 15);

    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }


}
