package homework_6.pageObject.mail.ru;

import homewrok_6.EmailPage;
import homewrok_6.HomePage;
import homewrok_6.InboxPage;
import homewrok_6.TrashPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class Exercise_3 extends BaseTest {


    @Test
    public void MailTest() {

        // 1. Login
        HomePage homePage = new HomePage(driver);
        homePage.open();
        homePage.login(MAIL_LOGIN, MAIL_PASSWORD);

        // 2. verify success entrance
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.validatePage();

        // 3. create new email

        inboxPage.pressCreateEmailButton();
        EmailPage emailPage = new EmailPage(driver);
        emailPage.fillToField(MAIL_SEND_ADDRESS);
        emailPage.fillSubjectField(MAIL_SEND_NAME);
        emailPage.fillEmailText(MAIL_SEND_NAME);

        // 4. Send email
        emailPage.pressSendButton();
        emailPage.pressCloseButton();

        // 5. Verify email in inbox folder
        inboxPage.inboxClick();
        inboxPage.clickSelfMails();
        inboxPage.findEmailInInbox(0);

        // 6. verify content,address and topic of email
        assertEquals(MAIL_SEND_NAME, inboxPage.getMailSendAddress(0));
        assertEquals(MAIL_SEND_NAME, inboxPage.getMailSubject(0));
        assertEquals(MAIL_SEND_NAME, inboxPage.getMailText(0));

        // 7. delete email
        inboxPage.openEmail(0);
        emailPage.pressDeleteButton();

        // 8. Verify email in
        TrashPage trashPage = new TrashPage(driver);
        trashPage.trashClick();

        assertEquals(MAIL_SEND_NAME, trashPage.getMailSendAddress(0));
        assertEquals(MAIL_SEND_NAME, trashPage.getMailSubject(0));
        assertEquals(MAIL_SEND_NAME, trashPage.getMailText(0));

        // 9. Exit account
        homePage.exitAccount();
    }


}
