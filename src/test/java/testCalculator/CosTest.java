package testCalculator;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CosTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Cosinus calculation");

    }

    @Test(testName = "Cosinus",
            dataProviderClass = TestDataProvider.class, dataProvider = "CosinusDataProvider")
    public void cosCalculation(double cosValue, double expected) {
        System.out.println("cos " + cosValue + " = " + expected);
        double result = calculator.cos(cosValue);
        Assert.assertEquals(result, expected, 0.0000001);
    }
}

