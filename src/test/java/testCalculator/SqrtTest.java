package testCalculator;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SqrtTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Square root");

    }

    @Test(testName = "Sqrt",
            dataProviderClass = TestDataProvider.class, dataProvider = "SqrtDataProvider")
    public void sqrtCalculation(double sqrtValue, double expected) {
        System.out.println("√ " + sqrtValue + " = " + expected);
        double result = calculator.sqrt(sqrtValue);
        Assert.assertEquals(result, expected, 0.0000001);
    }
}

