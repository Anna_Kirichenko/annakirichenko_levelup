package testCalculator;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class IsPositiveTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Positive");

    }

    @Test(testName = "isPositiveTest",
            dataProviderClass = TestDataProvider.class, dataProvider = "IsPositiveDataProvider")
    public void isPositive(long value, boolean expected) {
        System.out.println("Is Positive Calculator Test");
        boolean result = calculator.isPositive(value);
        Assert.assertEquals(result, expected);


    }
}