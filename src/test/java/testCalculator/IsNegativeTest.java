package testCalculator;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class IsNegativeTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Negative");

    }

    @Test(testName = "isNegativeTest",
            dataProviderClass = TestDataProvider.class, dataProvider = "IsNegativeDataProvider")
    public void isNegative(long value, boolean expected) {
        System.out.println("Is Negative Calculator Test");
        boolean result = calculator.isNegative(value);
        Assert.assertEquals(result, expected);


    }
}
