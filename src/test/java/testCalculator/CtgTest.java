package testCalculator;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CtgTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Cotangent calculation");

    }

    @Test(testName = "Cotangent",
            dataProviderClass = TestDataProvider.class, dataProvider = "CtgDataProvider")
    public void ctgCalculation(double ctgValue, double expected) {
        System.out.println("ctg " + ctgValue + " = " + expected);
        double result = calculator.ctg(ctgValue);
        Assert.assertEquals(result, expected, 0.0000001);
    }
}

