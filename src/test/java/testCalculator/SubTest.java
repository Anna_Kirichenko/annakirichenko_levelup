package testCalculator;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.*;

public class SubTest extends BaseCalculatorTest {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Substraction");

    }


    @Test(testName = "LongSubstraction",
            dataProviderClass = TestDataProvider.class, dataProvider = "SubLongDataProvider")
    public void substractionLongTest(long firstArg, long secondArg, long expected) {
        System.out.println(firstArg + "-" + secondArg + " = " + expected);
        long result = calculator.sub(firstArg,secondArg);
        Assert.assertEquals(result, expected);
    }

    @Test(testName = "DoubleSubstraction",
            dataProviderClass = TestDataProvider.class, dataProvider = "SubDoubleDataProvider")
    public void subtractionDoubleTest(double firstArg, double secondArg, double expected) {
        System.out.println(firstArg + "-" + secondArg + " = " + expected);

        double result = calculator.sub(firstArg,secondArg);
        Assert.assertEquals(result, expected, 0.0000001);
    }


}
