package testCalculator;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MultTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Multiplication");
    }

     @Test(testName = "DoubleMultiplication",
             dataProviderClass = TestDataProvider.class, dataProvider = "MultDoubleDataProvider")
     public void multDoubleTest(double firstArg, double secondArg, double expected) {
         System.out.println(firstArg + "*" + secondArg + " = " + expected);
        double result = calculator.mult(firstArg,secondArg);
        Assert.assertEquals(result,expected,0.0000001);

    }

    @Test(testName = "LongMultiplication",
            dataProviderClass = TestDataProvider.class, dataProvider = "MultLongDataProvider")
    public void multLongTest(long firstArg, long secondArg, long expected) {
        System.out.println(firstArg + "*" + secondArg + " = " + expected);
        long result = calculator.mult(firstArg,secondArg);
        Assert.assertEquals(result,expected);
    }


}
