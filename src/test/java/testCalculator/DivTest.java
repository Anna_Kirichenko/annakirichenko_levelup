package testCalculator;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class DivTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Division");

    }

    @Test(testName = "divDoubleTest",
            dataProviderClass = TestDataProvider.class, dataProvider = "DivDoubleDataProvider")
    public void divDoubleTest(double firstArg, double secondArg, double expected) {
        System.out.println(firstArg + "/" + secondArg + " = " + expected);
        double result = calculator.div(firstArg, secondArg);
        Assert.assertEquals(result, expected, 0.0000001);

    }

    @Test(testName = "divLongTest",
            dataProviderClass = TestDataProvider.class, dataProvider = "DivLongDataProvider")
    public void divLongTest(long firstArg, long secondArg, long expected) {
        System.out.println(firstArg + "/" + secondArg + " = " + expected);
        long result = calculator.div(firstArg, secondArg);
        Assert.assertEquals(result, expected, 0.0000001);
    }


}

