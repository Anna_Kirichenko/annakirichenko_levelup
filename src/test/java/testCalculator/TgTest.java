package testCalculator;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TgTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Tangents calculation");

    }

    @Test(testName = "Tangents",
            dataProviderClass = TestDataProvider.class, dataProvider = "TgDataProvider")
    public void tgCalculation(double tgValue, double expected) {
        System.out.println("tg " + tgValue + " = " + expected);
        double result = calculator.tg(tgValue);
        Assert.assertEquals(result, expected, 0.0000001);
    }
}
