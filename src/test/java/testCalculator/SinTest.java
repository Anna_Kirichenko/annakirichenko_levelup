package testCalculator;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SinTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Sinus calculation");

    }

    @Test(testName = "Sinus",
            dataProviderClass = TestDataProvider.class, dataProvider = "SinusDataProvider")
    public void sinCalculation(double SinValue, double expected) {
        System.out.println("sin " + SinValue + " = " + expected);
        double result = calculator.sin(SinValue);
        Assert.assertEquals(result, expected, 0.0000001);
    }
}
