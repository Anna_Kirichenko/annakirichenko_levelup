package testCalculator;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class
PowTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Exponentiation");

    }

    @Test(testName = "Pow",
            dataProviderClass = TestDataProvider.class, dataProvider = "PowDataProvider")
    public void powCalculation(double value, double pow, double expected) {
        System.out.println(value + " ^ " + pow + " = " + expected);
        double result = calculator.pow(value, pow);
        Assert.assertEquals(result, expected, 0.0000001);
    }
}