package testCalculator;

import org.testng.annotations.DataProvider;

public class TestDataProvider {
    @DataProvider
    public Object[][] AddDoubleDataProvider() {
        return new Object[][]{
                {1.5D, 1.5D, 3.0D},
                {3.5, 5.4, 8.9},
                {0, 0, 0},
                {-1.5, -2.3, -3.8},
                {-2.0, -7.0, -9.0}
        };
    }

    @DataProvider
    public Object[][] AddLongDataProvider() {
        return new Object[][]{
                {4000000000000000000L, 4000000000000000000L, 8000000000000000000L},
                {4000000000000000000L, 0, 4000000000000000000L},
                {0, 0, 0},
                {-30, 40, 10},
                {-4L, -4L, -8L}
        };


    }

    @DataProvider
    public Object[][] SubDoubleDataProvider() {
        return new Object[][]{
                {1.5D, 1.5D, 0.0D},
                {3.5, 5.4, -1.9},
                {0, 0, 0},
                {-1.5, -2.3, 0.8},
                {-2.0, -7.0, 5}
        };
    }

    @DataProvider
    public Object[][] SubLongDataProvider() {
        return new Object[][]{
                {9000000000000000000L, 4000000000000000000L, 5000000000000000000L},
                {4000000000000000000L, 0, 4000000000000000000L},
                {0, 0, 0},
                {-30, 40, -70},
                {-4L, -4L, 0}
        };


    }

    @DataProvider
    public Object[][] MultLongDataProvider() {
        return new Object[][]{
                {2000000000000000000L, 2, 4000000000000000000L},
                {4000000000000000000L, 0, 0},
                {0, 0, 0},
                {-30, 40, -1200},
                {-4L, -4L, 16L}
        };


    }

    @DataProvider
    public Object[][] MultDoubleDataProvider() {
        return new Object[][]{
                {1.5D, 1.5D, 2.0D},
                {3.5, -5.4, -19},
                {0, 0, 0},
                {-1.5, -2.3, 3.0D},
                {-2.0, 0, 0}
        };
    }

    @DataProvider
    public Object[][] DivDoubleDataProvider() {
        return new Object[][]{
                {24.0, 2.0, 12.0},
                {27.5, 2.2, 12.5},
                {1, 0, Double.POSITIVE_INFINITY},
                {-5, 0, Double.NEGATIVE_INFINITY},
                {0, 10, 0},
                {50, 1, 50},
                {-0.8, -0.2, 4},
                {-25.0, 5.0, -5.0},
                {1.11111111E8, 9.0, 12345679.0}
        };
    }

    @DataProvider
    public Object[][] DivLongDataProvider() {
        return new Object[][]{
                {800000000000L, 2L, 400000000000L},
                {800000000000L, 1, 800000000000L},
                {0, 2L, 0},
                {-500, -2, 250},
                {-18L, 2L, -9L}
        };
    }

    @DataProvider
    public Object[][] SinusDataProvider() {
        return new Object[][]{
                {0, 0},
                {30, -0.9880316241},
                {-30, 0.9880316241},
                {90.2, 0.7871577652},


        };
    }

    @DataProvider
    public Object[][] CosinusDataProvider() {
        return new Object[][]{
                {0, 1},
                {45, 0.5253219888},
                {-90, -0.4480736161},
                {30.23, 0.3754384560}


        };
    }

    @DataProvider
    public Object[][] TgDataProvider() {
        return new Object[][]{
                {0, 0},
                {30, -6.4053311966},
                {-50, 0.2719006120},
                {30.23, 0.5990756878}


        };
    }

    @DataProvider
    public Object[][] CtgDataProvider() {
        return new Object[][]{
                {0, Double.POSITIVE_INFINITY},
                {360, -0.2958456980},
                {-180, -0.7469988144},
                {45.89, -0.3502601083}


        };
    }

    @DataProvider
    public Object[][] SqrtDataProvider() {
        return new Object[][]{
                {49, 7},
                {0, 0},
                {0.25, 0.5},
                {-9, 3}


        };
    }

    @DataProvider
    public Object[][] PowDataProvider() {
        return new Object[][]{
                {0, 2, 0},
                {2, 0, 1},
                {100, 2, 10000},
                {-15, 3, -3375},
                {-15, -3, -0.0002962962962962963},
                {1.1, 1.1, 	1.1},
                {10, -2, 0.01}


        };
    }
    @DataProvider
    public Object[][] IsPositiveDataProvider() {
        return new Object[][]{
                {0, false},
                {-1, false},
                {1, true}

        };
    }
    @DataProvider
    public Object[][] IsNegativeDataProvider() {
        return new Object[][]{
                {0, false},
                {-1, true},
                {1, false}

        };
    }
}

