package testCalculator;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.*;

public class SumTest extends BaseCalculatorTest {
    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("Addition");

    }


    @Test(testName = "DoubleAddition",
            dataProviderClass = TestDataProvider.class, dataProvider = "AddDoubleDataProvider")
    public void sumDoubleTest(double firstArg, double secondArg, double expected) {
        System.out.println(firstArg + "+" + secondArg + " = " + expected);
        double result = calculator.sum(firstArg, secondArg);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @Test(testName = "LongAddition",
            dataProviderClass = TestDataProvider.class, dataProvider = "AddLongDataProvider")
    public void sumLongTest(long firstArg, long secondArg, long expected) {
        System.out.println(firstArg + "+" + secondArg + " = " + expected);
        long result = calculator.sum(firstArg,secondArg);
        Assert.assertEquals(result, expected);
    }


}
