package testCalculator;

import com.epam.tat.module4.Calculator;
import org.testng.annotations.*;

public abstract class BaseCalculatorTest {
    protected Calculator calculator;
    double firstArgDouble;
    double secondArgDouble;
    long firstArgLong;
    long secondArgLong;


    @BeforeMethod
    public void setUpBeforeMethod() {
        System.out.println("setUpBeforeMethod");
        calculator = new Calculator();
    }

    @AfterMethod
    public void tearDownAfterMethod() {
        System.out.println("tearDownAfterMethod");
        calculator = null;
    }
    @AfterTest
    public void tearDownAfterTest() {
        System.out.println();
    }


}