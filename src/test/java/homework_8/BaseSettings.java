package homework_8;


import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeMethod;

import static homework_8.EndPoints.BASE_REQ_RES_ENDPOINT;
import static io.restassured.RestAssured.oauth2;


public class BaseSettings {
    private static final String TOKEN = "7jwl4EHik3FTQFPhXxdnbccB0rsjRzMp1A7S";
    RequestSpecification rqSpec;
    ResponseSpecification respSpec;

    @BeforeMethod
    public void setUp() {
        rqSpec = new RequestSpecBuilder()
                .setBaseUri(BASE_REQ_RES_ENDPOINT)
                .log(LogDetail.ALL)
                .setAuth(oauth2(TOKEN))
                .build();

        respSpec = new ResponseSpecBuilder()


                .log(LogDetail.ALL)
                .expectContentType(ContentType.JSON)
                .build();


    }

}
