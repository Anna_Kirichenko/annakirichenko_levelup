package homework_8;

import com.sun.org.glassfish.gmbal.Description;
import homework_8.comment.Comment;
import homework_8.comment.CommentResponse;
import homework_8.comment.ListCommentsResponse;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CommentsTests extends BaseSettings {
    //1
    @Test
    @Description("GET list of comments")
    public void getListOfComments() {
        ListCommentsResponse commentsResponse = given()
                .spec(rqSpec)
                .when()
                .get(EndPoints.COMMENTS_ENDPOINT)
                .as(ListCommentsResponse.class);
        System.out.println(commentsResponse);
        assertThat(commentsResponse.getMeta().getCode(), equalTo(200));
        assertThat(commentsResponse.getResult().isEmpty(), is(false));
        assertThat(commentsResponse.getResult(), is(notNullValue()));


    }


    /*   //2 Тест опциональный
       @Test
       @Description("Get list of comments by title containing Montana.")
       public void getListOfCommentsByTitle() {

       }
   */
    //3
    @Test(dataProvider = "commentDataProvider", dataProviderClass = TestDataProvider.class)
    @Description("POST create a new comment.")
    public void postComment(Comment request, CommentResponse response) {
        CommentResponse responseBody = given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(request)
                .when()
                .post(EndPoints.COMMENTS_ENDPOINT)
                .as(CommentResponse.class);
        System.out.println(responseBody);
        assertThat(responseBody.getResult().getPost_id(), equalTo(request.getPost_id()));
        assertThat(responseBody.getResult().getComment_name(), equalTo(request.getComment_name()));
        assertThat(responseBody.getResult().getEmail(), equalTo(request.getEmail()));


        assertThat(responseBody.getMeta(), samePropertyValuesAs(response.getMeta(), "message"));
        assertThat(responseBody.getResult(), samePropertyValuesAs(response.getResult(), "id", "body"));
    }

    //4
    @Test(dataProvider = "getCommentDataProviderById", dataProviderClass = TestDataProvider.class)
    @Description("GET return  comment by ID")
    public void getOneComment(long expectedId, CommentResponse expectedComment) {
        CommentResponse responseBody = given()
                .spec(rqSpec)
                .pathParam("id", expectedId)
                .when()
                .get(EndPoints.GET_COMMENTS_ENDPOINT)
                .as(CommentResponse.class);
        System.out.println(responseBody);
        assertThat(responseBody.getResult().getComment_id(), equalTo(expectedId));
        assertThat(responseBody.getResult().getPost_id(), equalTo(expectedComment.getResult().getPost_id()));
    }

    //5
    @Test(dataProvider = "deleteCommentDataProviderById", dataProviderClass = TestDataProvider.class)
    @Description("DELETE delete comment")
    public void deleteComment(long expectedId) {
        given()
                .spec(rqSpec)
                .pathParam("id", expectedId)
                .when()
                .delete(EndPoints.GET_COMMENTS_ENDPOINT)
                .then()
                .statusCode(200);
    }

    //6
    @Test(dataProvider = "putCommentDataProvider", dataProviderClass = TestDataProvider.class)
    @Description("PUT update comment")
    public void updateComment(long commentIdForUpdate, Comment updatedComment) {
        CommentResponse responseBody= given()
                .spec(rqSpec)
                .pathParam("id", commentIdForUpdate)
                .body(updatedComment)
                .when()
                .contentType(ContentType.JSON)
                .put(EndPoints.GET_COMMENTS_ENDPOINT)
                .as(CommentResponse.class);
        System.out.println(responseBody);
        assertThat(responseBody.getResult().getComment_body(), equalTo(updatedComment.getComment_body()));

    }
    //7  Тест про старницу опциональный
}
