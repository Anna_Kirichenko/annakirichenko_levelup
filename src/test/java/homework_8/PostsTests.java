package homework_8;

import com.sun.org.glassfish.gmbal.Description;
import homework_8.post.ListPostResponse;
import homework_8.post.Post;
import homework_8.post.PostResponse;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class PostsTests extends BaseSettings {
    //1
    @Test
    @Description("GET list of posts")
    public void getListOfPosts() {
        ListPostResponse postResponse = given()
                .spec(rqSpec)
                .when()
                .get(EndPoints.POSTS_ENDPOINT)
                .as(ListPostResponse.class);
        System.out.println(postResponse);
        assertThat(postResponse.getMeta().getCode(), equalTo(200));
        assertThat(postResponse.getResult().isEmpty(), is(false));
        assertThat(postResponse.getResult(), is(notNullValue()));


    }

  /*  //2  Опициональный тест
    @Test
    @Description("list of posts with title contains Voluptates optio atque. ")
    public void getListOfPostsByTitle() {

    }
*/
    //3
    @Test(dataProvider = "postDataProvider", dataProviderClass = TestDataProvider.class)
    @Description("POST create a new post.")
    public void posPost(Post request, PostResponse response) {
        PostResponse responseBody = given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(request)
                .when()
                .post(EndPoints.POSTS_ENDPOINT)
                .as(PostResponse.class);
        System.out.println(responseBody);
        assertThat(responseBody.getResult().getPost_title(), equalTo(request.getPost_title()));


        assertThat(responseBody.getMeta(), samePropertyValuesAs(response.getMeta(), "message"));
        assertThat(responseBody.getResult(), samePropertyValuesAs(response.getResult(), "id"));
    }


    //4
    @Test(dataProvider = "getPostDataProviderById", dataProviderClass = TestDataProvider.class)
    @Description("GET return  post by ID")
    public void getOnePost(long expectedId, PostResponse expectedPost) {
        PostResponse responseBody = given()
                .spec(rqSpec)
                .pathParam("id", expectedId)
                .when()
                .get(EndPoints.GET_POSTS_ENDPOINT)
                .as(PostResponse.class);
        System.out.println(responseBody);
        assertThat(responseBody.getResult().getPost_id(), equalTo(expectedId));
        assertThat(responseBody.getResult().getPost_title(), equalTo(expectedPost.getResult().getPost_title()));
    }

    //5
    @Test(dataProvider = "deletePostDataProviderById", dataProviderClass = TestDataProvider.class)
    @Description("DELETE delete post")
    public void deletePost(long expectedId) {
        given()
                .spec(rqSpec)
                .pathParam("id", expectedId)
                .when()
                .delete(EndPoints.GET_POSTS_ENDPOINT)
                .then()
                .statusCode(200);
    }


    //6
    @Test (dataProvider = "putPostDataProvider", dataProviderClass = TestDataProvider.class)
    @Description("PUT update post")
    public void updatePost(long postIdForUpdate, Post updatedPost) {
PostResponse responseBody= given()
        .spec(rqSpec)
        .pathParam("id", postIdForUpdate)
        .body(updatedPost)
        .when()
        .contentType(ContentType.JSON)
        .put(EndPoints.GET_POSTS_ENDPOINT)
        .as(PostResponse.class);
        System.out.println(responseBody);
        assertThat(responseBody.getResult().getPost_title(), equalTo(updatedPost.getPost_title()));
        assertThat(responseBody.getResult().getPost_body(), equalTo(updatedPost.getPost_body()));
    }
    //7  Тест про старницу опциональный
}
