package homework_8;

import com.sun.org.glassfish.gmbal.Description;

import homework_8.user.ListUsersResponse;
import homework_8.user.User;
import homework_8.user.UserResponse;
import io.restassured.http.ContentType;
import io.restassured.internal.mapping.GsonMapper;
import io.restassured.path.json.mapper.factory.DefaultGsonObjectMapperFactory;
import org.testng.annotations.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.testng.Assert.assertEquals;

public class UsersTests extends BaseSettings {
    //1
    @Test
    @Description("GET list all users")
    public void getListOfUsers() {
        ListUsersResponse usersResponse = given()
                .spec(rqSpec)
                .when()
                .get(EndPoints.USERS_ENDPOINT)
                .as(ListUsersResponse.class);
        System.out.println(usersResponse);
        assertThat(usersResponse.getMeta().getCode(), equalTo(200));
        assertThat(usersResponse.getResult().isEmpty(), is(false));
        assertThat(usersResponse.getResult(), is(notNullValue()));

    }

    //2
    @Test(dataProvider = "userFirstNameDataProvider", dataProviderClass = TestDataProvider.class)
    @Description("GET list of all users with first_name contains john.")
    public void getUserByFirstName(String firstName,String expectedName) {
        ListUsersResponse responseBody = given()
                .spec(rqSpec)
        .when()
               .param(firstName, expectedName)
                .get(EndPoints.USERS_ENDPOINT)
                .as(ListUsersResponse.class, new GsonMapper(new DefaultGsonObjectMapperFactory()));

        System.out.println(responseBody);

        assertThat(responseBody.getMeta().getCode(), equalTo(200));
        assertThat(responseBody.getResult().isEmpty(), is(false));
        assertThat(responseBody.getResult(), is(notNullValue()));
        List<String> expectedENames = Arrays.asList("John");
        List<String> actualNames = new ArrayList<>();
        List<User> data = responseBody.getResult();
        for (User user : data) {
            actualNames.add(user.getFirstName());
        }
        assertEquals(actualNames, expectedENames);
    }

    //3
    @Test(dataProvider = "userdataProvider", dataProviderClass = TestDataProvider.class)
    @Description("POST create a new user.")
    public void postUser(User request, UserResponse response) {
        UserResponse responseBody = given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(request)
                .when()
                .post(EndPoints.USERS_ENDPOINT)
                .as(UserResponse.class);
        System.out.println(responseBody);
        assertThat(responseBody.getResult().getFirstName(), equalTo(request.getFirstName()));
        assertThat(responseBody.getResult().getLastName(), equalTo(request.getLastName()));

        assertThat(responseBody.getMeta(), samePropertyValuesAs(response.getMeta(), "message"));
        assertThat(responseBody.getResult(), samePropertyValuesAs(response.getResult(), "id", "dob", "phone", "website", "address"));
    }

    //4
    @Test(dataProvider = "getUserDataProviderById", dataProviderClass = TestDataProvider.class)
    @Description("GET return  user by ID")
    public void getOneUserById(long expectedId, UserResponse expectedUser) {
        UserResponse responseBody = given()
                .spec(rqSpec)
                .pathParam("id", expectedId)
                .when()
                .get(EndPoints.GET_USER_ENDPOINT)
                .as(UserResponse.class);
        System.out.println(responseBody);
        assertThat(responseBody.getResult().getId(), equalTo(expectedId));
        assertThat(responseBody.getResult().getFirstName(), equalTo(expectedUser.getResult().getFirstName()));
        assertThat(responseBody.getResult().getEmail(), equalTo(expectedUser.getResult().getEmail()));
    }


    //5
    @Test(dataProvider = "deleteUserDataProviderById", dataProviderClass = TestDataProvider.class)
    @Description("DELETE delete user")
    public void deleteOneUser(long expectedId) {
        given()
                .spec(rqSpec)
                .pathParam("id", expectedId)
                .when()
                .delete(EndPoints.GET_USER_ENDPOINT)
                .then()
                .statusCode(200);
    }

    //6
    @Test
    @Description("get page")
    public void getpage() {

        ListUsersResponse responseBody = given()
                .spec(rqSpec)
                .when()
                .param("page", 5)
                .get(EndPoints.USERS_ENDPOINT)
                .as(ListUsersResponse.class, new GsonMapper(new DefaultGsonObjectMapperFactory()));

        System.out.println(responseBody);

        assertThat(responseBody.getMeta().getCode(), equalTo(200));
        assertThat(responseBody.getResult().isEmpty(), is(false));
        assertThat(responseBody.getResult(), is(notNullValue()));

    }

    //7
    @Test(dataProvider = "putUserdataProvider", dataProviderClass = TestDataProvider.class)
    @Description("PUT update user")
    public void updateUser(long userIdForUpdate, User updatedUser) {

        UserResponse responseBody = given()
                .spec(rqSpec)
                .pathParam("id", userIdForUpdate)
                .body(updatedUser)
                .when()
                .contentType(ContentType.JSON)
                .put(EndPoints.GET_USER_ENDPOINT)
                .as(UserResponse.class);
        System.out.println(responseBody);
        assertThat(responseBody.getResult().getFirstName(), equalTo(updatedUser.getFirstName()));
        assertThat(responseBody.getResult().getLastName(), equalTo(updatedUser.getLastName()));

    }

}

