package homework_8;

public final class EndPoints {

    public static final String BASE_REQ_RES_ENDPOINT = "https://gorest.co.in/public-api";
    public static final String GET_USER_ENDPOINT = "/users/{id}";
    public static final String USERS_ENDPOINT = "/users";
    public static final String POSTS_ENDPOINT = "/posts";
    public static final String GET_POSTS_ENDPOINT = "/posts/{id}";
    public static final String COMMENTS_ENDPOINT = "/comments";
    public static final String GET_COMMENTS_ENDPOINT = "/comments/{id}";


    private EndPoints() {

    }
}
