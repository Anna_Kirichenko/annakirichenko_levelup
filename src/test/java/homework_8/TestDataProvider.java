package homework_8;

import homework_8.comment.Comment;
import homework_8.comment.CommentResponse;
import homework_8.post.Post;
import homework_8.post.PostResponse;
import homework_8.user.User;
import homework_8.user.UserResponse;
import org.testng.annotations.DataProvider;

public class TestDataProvider extends BaseSettings {
    //User Data provider
    @DataProvider
    public Object[][] userdataProvider() {
        return new Object[][]{
                {new User("John", "Wick", "male", "johnWick@mail.ru", "active"),
                        new UserResponse(new Meta(true, 201),
                                new User("John", "Wick", "male", "johnWick@mail.ru", "active"))},
        };


    }

    @DataProvider
    public Object[][] userFirstNameDataProvider() {
        return new Object[][]{
                {"first_name", "John"}
        };


    }

    @DataProvider
    public Object[][] getUserDataProviderById() {
        return new Object[][]{
                {83744,
                        new UserResponse(new Meta(true, 200),
                                new User("John", "Wick", "male", "johnWick@mail.ru", "active"))}

        };


    }

    @DataProvider
    public Object[][] deleteUserDataProviderById() {
        return new Object[][]{
                {11945}

        };
    }

    @DataProvider
    public Object[][] putUserdataProvider() {
        return new Object[][]{
                {85367, new User("Helen", "Wick", "female", "helenWick@mail.ru", "active")},
        };


    }

    //Post Data provider
    @DataProvider
    public Object[][] postDataProvider() {
        return new Object[][]{
                {new Post(83744, "blablabla", "blablabla"),
                        new PostResponse(new Meta(true, 201), new Post(83744, "blablabla", "blablabla"))},
        };
    }

    @DataProvider
    public Object[][] getPostDataProviderById() {
        return new Object[][]{
                {16047,
                        new PostResponse(new Meta(true, 200),
                                new Post(83744, "blablabla", "blablabla"))}

        };
    }

    @DataProvider
    public Object[][] deletePostDataProviderById() {
        return new Object[][]{
                {6714}

        };
    }

    @DataProvider
    public Object[][] putPostDataProvider() {
        return new Object[][]{
                {15930, new Post(30484, "per aspera ad astra", "cognosce te ipsum")},
        };


    }

    //Comments Data provider
    @DataProvider
    public Object[][] commentDataProvider() {
        return new Object[][]{
                {new Comment(16047, "John", "johnWick@mail.ru", "blablabla"),
                        new CommentResponse(new Meta(true, 201), new Comment(16047, "John", "johnWick@mail.ru", "blablabla"))},
        };
    }

    @DataProvider
    public Object[][] getCommentDataProviderById() {
        return new Object[][]{
                {13633,
                        new CommentResponse(new Meta(true, 200),
                                new Comment(16047, "John", "johnWick@mail.ru", "blablabla"))}

        };
    }

    @DataProvider
    public Object[][] deleteCommentDataProviderById() {
        return new Object[][]{
                {5532}

        };
    }

    @DataProvider
    public Object[][] putCommentDataProvider() {
        return new Object[][]{
                {5879, new Comment(8138, "Zena Labadie", "amelie05@stracke.com", "123456789")}
        };
    }
}