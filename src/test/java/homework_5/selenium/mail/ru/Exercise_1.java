package homework_5.selenium.mail.ru;

import io.qameta.allure.Issue;
import io.qameta.allure.Story;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class Exercise_1 extends BaseTest {


    @Test
    @Story("Mail Test 1")
    public void MailTest() {

        String title = driver.getTitle();
        assertEquals(title, "Mail.ru: почта, поиск в интернете, новости, игры");

        // 1. Login
        WebElement userNameTextFieldById = driver.findElement(By.id("mailbox:login"));
        userNameTextFieldById.sendKeys(MAIL_LOGIN);
        WebElement loginButton = driver.findElement(By.xpath("//input[@class='o-control']"));
        loginButton.click();
        WebElement passwordTextField = wait.until(ExpectedConditions.elementToBeClickable(By.id("mailbox:password")));
        passwordTextField.sendKeys(MAIL_PASSWORD);
        loginButton.click();

        // 2. verify success entrance
        WebElement inBox = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[class='nav__folder-name__txt']")));
        assertEquals(inBox.getText(), "Входящие");

        // 3. create new email

        driver.findElement(By.xpath("//*[@class='compose-button__txt']")).click();
        WebElement to = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-type='to'] input")));
        to.sendKeys(MAIL_SEND_ADDRESS);
        WebElement subject = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name='Subject']")));
        subject.sendKeys(MAIL_SUBJECT);
        WebElement emailText = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[role='textbox']")));
        emailText.sendKeys(MAIL_TEXT);

        // 4. Save draft of email
        WebElement saveButton = driver.findElement(By.xpath("//*[@title='Сохранить']"));
        saveButton.click();
        WebElement closeButton = driver.findElement(By.xpath("//*[@title='Закрыть']"));
        closeButton.click();

        // 5. verify email in drafts
        WebElement drafts = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title^='Черновики']")));
        drafts.click();
        wait.until(ExpectedConditions.titleIs("Черновики - Почта Mail.ru"));
        WebElement draftEMail = driver.findElements(By.xpath("//*[@class='llc__item llc__item_correspondent']")).get(0);

        // 6. verify content,address and topic of email
        assertEquals(MAIL_SEND_ADDRESS, draftEMail.getText());
        assertEquals(MAIL_SUBJECT, driver.findElements(By.className("llc__subject")).get(0).getText());
        assertEquals(MAIL_TEXT, driver.findElements(By.className("ll-sp__normal")).get(0).getText());

        // 7. Send draft
        draftEMail.click();
        WebElement sendButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@title='Отправить']")));
        sendButton.click();
        closeButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title='Закрыть']")));
        closeButton.click();

        // 8. Verify that draft box is empty
        WebElement draftEmpty = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("octopus__title")));
        assertEquals("У вас нет незаконченных\nили неотправленных писем", draftEmpty.getText());

        // 9. Verify email in send folder
        WebElement send = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title^='Отправленные']")));
        send.click();
        wait.until(ExpectedConditions.titleIs("Отправленные - Почта Mail.ru"));
        WebElement sendEmail = driver.findElements(By.xpath("//*[@class='llc__item llc__item_correspondent']")).get(0);

        // 10. Exit account
        WebElement exitButton = driver.findElement(By.id("PH_logoutLink"));
        exitButton.click();
        wait.until(ExpectedConditions.titleIs("Mail.ru: почта, поиск в интернете, новости, игры"));

    }


}
