package homework_5.selenium.mail.ru;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class Exercise_2 extends BaseTest {


    @Test
    public void MailTest() {
        long startTime;
        long endTime;
        String title = driver.getTitle();
        assertEquals(title, "Mail.ru: почта, поиск в интернете, новости, игры");

        // 1. Login
        WebElement userNameTextFieldById = driver.findElement(By.id("mailbox:login"));
        userNameTextFieldById.sendKeys(MAIL_LOGIN);
        WebElement loginButton = driver.findElement(By.xpath("//input[@class='o-control']"));
        loginButton.click();
        WebElement passwordTextField = wait.until(ExpectedConditions.elementToBeClickable(By.id("mailbox:password")));
        passwordTextField.sendKeys(MAIL_PASSWORD);
        loginButton.click();

        // 2. verify success entrance
        WebElement inBox = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[class='nav__folder-name__txt']")));
        assertEquals(inBox.getText(), "Входящие");

        // 3. create new email
        driver.findElement(By.xpath("//*[@class='compose-button__txt']")).click();
        WebElement to = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-type='to'] input")));
        to.sendKeys(MAIL_SEND_ADDRESS);
        WebElement subject = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name='Subject']")));
        subject.sendKeys(MAIL_TESTSUBJECT);
        WebElement emailText = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[role='textbox']")));
        emailText.sendKeys(MAIL_TESTSUBJECT);

        // 4. Send email
        WebElement sendButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@title='Отправить']")));
        sendButton.click();
        WebElement closeButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title='Закрыть']")));
        closeButton.click();

        // 5. Verify email in send folder
        WebElement sendLable = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title^='Отправленные']")));
        sendLable.click();
        wait.until(ExpectedConditions.titleIs("Отправленные - Почта Mail.ru"));
        WebElement sendEmail = driver.findElements(By.xpath("//*[@class='llc__item llc__item_correspondent']")).get(0);

        // 6. Verify email in Test folder

        WebElement send = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title^='Test']")));
        send.click();
        wait.until(ExpectedConditions.titleContains("Test - Почта Mail.ru"));
        WebElement testEmail = driver.findElements(By.cssSelector("div.llc__item_correspondent")).get(0);
        //List<WebElement> list = driver.findElements(By.cssSelector("div.llc__item_unread"));
        // int count = list.size();
        // wait.until(ExpectedConditions.titleIs("(" + count + ") Test - Почта Mail.ru"));


        // 7. verify content,address and topic of email
        assertEquals(MAIL_SEND_NAME, testEmail.getText());
        assertEquals(MAIL_TESTSUBJECT, driver.findElements(By.className("llc__subject")).get(0).getText());
        assertEquals(MAIL_TESTSUBJECT, driver.findElements(By.className("ll-sp__normal")).get(0).getText());

        // 8. Exit account
        WebElement exitButton = driver.findElement(By.id("PH_logoutLink"));
        exitButton.click();
        wait.until(ExpectedConditions.titleIs("Mail.ru: почта, поиск в интернете, новости, игры"));

    }


}
