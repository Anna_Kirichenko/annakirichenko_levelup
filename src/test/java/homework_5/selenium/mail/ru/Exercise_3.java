package homework_5.selenium.mail.ru;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class Exercise_3 extends BaseTest {


    @Test
    public void MailTest() {
        long startTime;
        long endTime;
        String title = driver.getTitle();
        assertEquals(title, "Mail.ru: почта, поиск в интернете, новости, игры");

        // 1. Login
        WebElement userNameTextFieldById = driver.findElement(By.id("mailbox:login"));
        userNameTextFieldById.sendKeys(MAIL_LOGIN);
        WebElement loginButton = driver.findElement(By.xpath("//input[@class='o-control']"));
        loginButton.click();
        WebElement passwordTextField = wait.until(ExpectedConditions.elementToBeClickable(By.id("mailbox:password")));
        passwordTextField.sendKeys(MAIL_PASSWORD);
        loginButton.click();

        // 2. verify success entrance
        WebElement inBox = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[class='nav__folder-name__txt']")));
        assertEquals(inBox.getText(), "Входящие");

        // 3. create new email
        driver.findElement(By.xpath("//*[@class='compose-button__txt']")).click();
        WebElement to = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-type='to'] input")));
        to.sendKeys(MAIL_SEND_ADDRESS);
        WebElement subject = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name='Subject']")));
        subject.sendKeys(MAIL_SEND_NAME);
        WebElement emailText = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[role='textbox']")));
        emailText.sendKeys(MAIL_SEND_NAME);

        // 4. Send email
        WebElement sendButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@title='Отправить']")));
        sendButton.click();
        WebElement closeButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title='Закрыть']")));
        closeButton.click();

        // 5. Verify email in inbox folder
        WebElement inboxFolder = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@href='/inbox/']")));
        inboxFolder.click();
        wait.until(ExpectedConditions.titleIs("Входящие - Почта Mail.ru"));
        WebElement selfEmails = driver.findElements(By.xpath("//*[@class='mt-h-c__content']")).get(0);

        // 6. verify content,address and topic of email
        selfEmails.click();
        wait.until(ExpectedConditions.titleContains("Входящие"));
        List<WebElement> list = driver.findElements(By.xpath("//div[@class='llc__item llc__item_correspondent llc__item_unread']"));
        WebElement firstEmail = list.get(0);
        assertEquals(MAIL_SEND_NAME, firstEmail.getText());
        assertEquals(MAIL_SEND_NAME, driver.findElements(By.className("llc__subject")).get(0).getText());
        assertEquals(MAIL_SEND_NAME, driver.findElements(By.className("ll-sp__normal")).get(0).getText());

        // 7. delete email
        firstEmail.click();
        WebElement deleteButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@title='Удалить']")));
        deleteButton.click();

        // 8. Verify email in
        WebElement trashFolder = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@href='/trash/']")));
        trashFolder.click();
        WebElement deletedMailName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='ll-crpt']")));
        assertEquals(MAIL_SEND_NAME, deletedMailName.getText());
        WebElement deletedMailSubject = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("llc__subject")));
        assertEquals(MAIL_SEND_NAME, deletedMailSubject.getText());
        WebElement deletedMailText = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ll-sp__normal")));
        assertEquals(MAIL_SEND_NAME, deletedMailText.getText());

        // 9. Exit account
        WebElement exitButton = driver.findElement(By.id("PH_logoutLink"));
        exitButton.click();
        wait.until(ExpectedConditions.titleIs("Mail.ru: почта, поиск в интернете, новости, игры"));

    }


}
