package homework_7.selenide.mail.ru;


import com.codeborne.selenide.Condition;
import homework_7.HomePage;
import homework_7.InboxPage;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static homework_7.HomePage.exitButton;
import static homework_7.InboxPage.*;
import static homework_7.InboxPage.textField;
import static org.testng.Assert.assertEquals;
@Feature("Selenide Alure")
public class Exercise_2 extends ConfigurationSelenideTest {


    @Test
    @Story("Mail Test 2")
    public void MailTest() {
        SoftAssert sa = new SoftAssert();
        // 1. Login
        HomePage homePage = new HomePage().open();
        homePage.login(MAIL_LOGIN, MAIL_PASSWORD);

        // 2. verify success entrance
        InboxPage inboxPage = new InboxPage();
        sa.assertTrue(inBox.isDisplayed(), "Входящие");


        // 3. create new email
        inboxPage.pressCreateEmailButton();
        toField.sendKeys(MAIL_SEND_ADDRESS);
        subjectField.sendKeys(MAIL_TESTSUBJECT);
        textField.sendKeys(MAIL_TESTSUBJECT);

        // 4. Send email
        sendButton.shouldBe(Condition.visible).click();
        closeButton.shouldBe(Condition.visible).click();

        // 5. Verify email in send folder
        send.shouldBe(Condition.visible).click();
        sendEmails.get(0).shouldBe(Condition.visible);

        // 6. Verify email in Test folder
        testfolderClick.shouldBe(Condition.visible).click();
        testEmail.get(0).shouldBe(Condition.visible);

        // 7. verify content,address and topic of email
        sa.assertEquals(MAIL_SEND_ADDRESS, testEmailSender.get(0));
        sa.assertEquals(MAIL_TESTSUBJECT, testMailSubject.get(0));
        sa.assertEquals(MAIL_TESTSUBJECT, testMailText.get(0));

        // 8. Exit account
        exitButton.shouldBe(Condition.visible).click();

    }


}
