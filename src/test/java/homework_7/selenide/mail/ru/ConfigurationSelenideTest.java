package homework_7.selenide.mail.ru;

import com.codeborne.selenide.Browsers;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;


public abstract class ConfigurationSelenideTest {


    public static final String MAIL_LOGIN = "anna_kirichenko_levelup@mail.ru";
    public static final String MAIL_PASSWORD = "Ktnj2020";
    public static final String MAIL_SEND_ADDRESS = "anna_kirichenko_levelup@mail.ru";
    public static final String MAIL_SEND_NAME = "Anna Kirichenko";
    public static final String MAIL_SUBJECT = "First email of John Doe";
    public static final String MAIL_TEXT = "Hi there. How are you?";
    public static final String MAIL_TESTSUBJECT = "Test";
    public static final String NO_MAILS_MESSAGE = "У вас нет незаконченных\nили неотправленных писем";
    @BeforeSuite
    public void beforeSuite() {
        SelenideLogger.addListener("AllureSelenide",
                new AllureSelenide().savePageSource(true).screenshots(true));


//        System.out.println();
//        System.out.println(System.getenv("KEY_UI_TEST"));
//        System.out.println(System.getenv("PROTECTED_VALUE"));
//        System.out.println(System.getenv("PROTECTED_MASKED"));
//        System.out.println();
    }


    @BeforeMethod
    public void setUp() {
        Configuration.browser = Browsers.CHROME;
        Configuration.timeout = 7500;
        Configuration.baseUrl = "https://mail.ru";
        Configuration.reportsFolder = "target/reports/tests";
        Configuration.startMaximized = true;
        Configuration.headless = true;
    }

    @AfterMethod
    public void tearDown() {
        Selenide.closeWindow();
    }



}
