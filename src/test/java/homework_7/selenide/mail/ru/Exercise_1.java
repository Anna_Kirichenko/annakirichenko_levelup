package homework_7.selenide.mail.ru;

import com.codeborne.selenide.Condition;
import homework_7.HomePage;
import homework_7.InboxPage;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static homework_7.HomePage.exitButton;
import static homework_7.InboxPage.*;

@Feature("Selenide Alure")
public class Exercise_1 extends ConfigurationSelenideTest {

    @Test
    @Story("Mail Test 1")
    @Issue("TS-01")
    public void MailTest() {
        SoftAssert sa = new SoftAssert();

        // 1. Login
        HomePage homePage = new HomePage().open();
        homePage.login(MAIL_LOGIN, MAIL_PASSWORD);

        // 2. verify success entrance
        InboxPage inboxPage = new InboxPage();
        sa.assertTrue(inBox.isDisplayed(), "Входящие");

        // 3. create new email
        inboxPage.pressCreateEmailButton();
        toField.sendKeys(MAIL_SEND_ADDRESS);
        subjectField.sendKeys(MAIL_SUBJECT);
        textField.sendKeys(MAIL_TEXT);

        // 4. Save draft of email
        saveButton.shouldBe(Condition.visible).click();
        closeButton.shouldBe(Condition.visible).click();

        // 5. verify email in drafts
        draftFolder.shouldBe(Condition.visible).click();

        // 6. verify content,address and topic of email
        sa.assertEquals(MAIL_SEND_ADDRESS, draftEmails.get(0));
        sa.assertEquals(MAIL_SUBJECT, mailSubject.get(0));
        sa.assertEquals(MAIL_TEXT, mailText.get(0));

        // 7. Send draft
        mailSender.get(0).shouldBe(Condition.visible).click();
        sendButton.shouldBe(Condition.visible).click();
        closeButton.shouldBe(Condition.visible).click();

        // 8. Verify that draft box is empty
        draftEmpty.shouldBe(Condition.visible);
        sa.assertEquals(NO_MAILS_MESSAGE, draftEmpty.getText());

        // 9. Verify email in send folder
        send.shouldBe(Condition.visible).click();
        sendEmails.get(0).shouldBe(Condition.visible);

        // 10. Exit account
        exitButton.shouldBe(Condition.visible).click();


    }


}
