package homework_7.selenide.mail.ru;


import com.codeborne.selenide.Condition;
import homework_7.HomePage;
import homework_7.InboxPage;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static homework_7.HomePage.exitButton;
import static homework_7.InboxPage.*;
import static org.testng.Assert.assertEquals;
@Feature("Selenide Alure")
public class Exercise_3 extends ConfigurationSelenideTest {


    @Test
    @Story("Mail Test 3")
    public void MailTest() {
        SoftAssert sa = new SoftAssert();
        // 1. Login
        HomePage homePage = new HomePage().open();
        homePage.login(MAIL_LOGIN, MAIL_PASSWORD);

        // 2. verify success entrance
        InboxPage inboxPage = new InboxPage();
        sa.assertTrue(inBox.isDisplayed(), "Входящие");

        // 3. create new email
        inboxPage.pressCreateEmailButton();
        toField.sendKeys(MAIL_SEND_ADDRESS);
        subjectField.sendKeys(MAIL_SEND_NAME);
        textField.sendKeys(MAIL_SEND_NAME);

        // 4. Send email
        sendButton.shouldBe(Condition.visible).click();
        closeButton.shouldBe(Condition.visible).click();

        // 5. Verify email in inbox folder
        inBox.shouldBe(Condition.visible).click();
        selfEmailslink.shouldBe(Condition.visible).click();
        testEmail.get(0).shouldBe(Condition.visible);

        // 6. verify content,address and topic of email
        sa.assertEquals(MAIL_SEND_NAME, inboxMailSender.get(0));
        sa.assertEquals(MAIL_SEND_NAME, inboxMailSubject.get(0));
        sa.assertEquals(MAIL_SEND_NAME, inboxMailText.get(0));

        // 7. delete email
        selfMails.get(0).shouldBe(Condition.visible).click();
        deleteButton.shouldBe(Condition.visible).click();
        inboxEmpty.shouldBe(Condition.visible);
        sa.assertEquals("Писем нет", draftEmpty.getText());

        // 8. Verify email in trash folder
        trashButton.shouldBe(Condition.visible).click();
        sa.assertEquals(MAIL_SEND_NAME, trashMailSender.get(0));
        sa.assertEquals(MAIL_SEND_NAME, trashMailSubject.get(0));
        sa.assertEquals(MAIL_SEND_NAME, trashMailText.get(0));

        // 9. Exit account
        exitButton.shouldBe(Condition.visible).click();
    }


}
