package homework_2.collectionPractice;

import java.util.*;

public class CollectionPractice {


    public static void main(String[] args) {
        List<Long> numbers = new ArrayList<>();
        for (long i = 0; i <= 100000; i++) {
            numbers.add(i);
        }

        System.out.println("Order before shuffling");
        for (int i = 0; i < 5; i++) {
            System.out.println(numbers.get(i) + " ");
        }

        Collections.shuffle(numbers);

        System.out.println("Order after shuffling");
        for (int i = 0; i < 5; i++) {
            System.out.println(numbers.get(i) + " ");
        }
       //Создаем уникальныйй список
        Set <Long> uniqueSet= new HashSet<>(numbers);
        System.out.println("All elements are unique:");
        System.out.println(numbers.size() == uniqueSet.size());

        Map<Integer, Set<Long>> sorted_map = new HashMap<>();

        Set<Long> number_2 = new HashSet<>();
        Set<Long> number_3 = new HashSet<>();
        Set<Long> number_5 = new HashSet<>();
        Set<Long> number_7 = new HashSet<>();

        for (Long number : numbers) {
            if (number % 2 == 0) {
                number_2.add(number);
            } else if
            (number % 3 == 0) {
                number_3.add(number);
            } else if
            (number % 5 == 0) {
                number_5.add(number);
            } else if
            (number % 7 == 0) {
                number_7.add(number);
            }
        }
        sorted_map.put(2, number_2);
        sorted_map.put(3, number_3);
        sorted_map.put(5, number_5);
        sorted_map.put(7, number_7);


    }
}


