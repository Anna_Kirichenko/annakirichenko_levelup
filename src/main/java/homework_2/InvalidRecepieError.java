package homework_2;

public class InvalidRecepieError extends Exception {
    public InvalidRecepieError() {
        super("Recipe is null.Please add ingredients in it.");
    }
}
