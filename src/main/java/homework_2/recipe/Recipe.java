package homework_2.recipe;

import homework_2.InvalidRecepieError;
import homework_2.ingredients.Ingredient;

import java.util.Comparator;
import java.util.List;

public class Recipe {
    String name;
    List<Ingredient> ingredientList;

    public Recipe(String name, List<Ingredient> productList) throws InvalidRecepieError {
        this.name = name;
        if(productList==null) throw new InvalidRecepieError();
        this.ingredientList = productList;
    }

    public String getName() {
        return name;
    }

    public List<Ingredient> getIngredientList() {
        return ingredientList;
    }

    @Override
    public String toString() {
        return getName() + " with " + ingredientList;
    }

    //Посчитать количество каллорий в блюде.
    public double countСalories(List<Ingredient> ingredientList) {
        double amountOfCalories = 0;
        for (Ingredient ingredient : ingredientList) {
            amountOfCalories += ingredient.getProduct().getCalories();
        }
        System.out.println("There are " + amountOfCalories + " calories in your dish.");
        return amountOfCalories;

    }

    //Сортировка овощей на основе одного из параметров
    public void sortVegetable(List<Ingredient> ingredientList) {


        ingredientList.sort(new Comparator<Ingredient>() {
            @Override
            public int compare(Ingredient o1, Ingredient o2) {


                return o1.getProduct().getName().compareTo(o2.getProduct().getName());
            }
        });

    }

    //Найти продукт у которого содержание жиров больше 100 и количетсво каллорий меньше 300
    public List findIngredient(double fats, double calories) {
        for (Ingredient ingredient : ingredientList) {
            if (ingredient.getProduct().getFats() > 100 && ingredient.getProduct().getCalories() < 300)
                System.out.println(ingredient.getProduct().getName());
            else
                System.out.println("Ingredient doesn't found");
            break;
        }
        return null;
    }
}
