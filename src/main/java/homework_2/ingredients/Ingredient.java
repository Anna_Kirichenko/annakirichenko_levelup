package homework_2.ingredients;


import homework_2.product.Product;


public class Ingredient {
    Product product;
    double weightInGram;

    public Ingredient(Product product, double weightInGram) {
        this.product = product;
        this.weightInGram = weightInGram;
    }

    public Product getProduct() {
        return product;
    }

    public double getWeightInGram() {
        return weightInGram;
    }

    @Override
    public String toString() {
        return product.getName();
    }
}
