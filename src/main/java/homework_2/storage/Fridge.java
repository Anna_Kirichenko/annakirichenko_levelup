package homework_2.storage;

import homework_2.FridgeException;
import homework_2.ingredients.Ingredient;

import java.util.List;

public class Fridge {
    List<Ingredient> productList;

    public Fridge(List<Ingredient> productList) throws FridgeException {
        if (productList==null)throw  new FridgeException();
        this.productList = productList;
    }

    public List<Ingredient> getProductList() {
        return productList;
    }

    public boolean hasIngredients(List<Ingredient> requiredIngredients) {
        for (Ingredient ingredient : requiredIngredients) {
            boolean hasIngredient = false;
            for (Ingredient availableIngredient : productList) {
                if (ingredient.getProduct().getName().equals(availableIngredient.getProduct().getName())) {
                    if (ingredient.getWeightInGram() <= availableIngredient.getWeightInGram()) {
                        hasIngredient = true;
                        double remainWeight = availableIngredient.getWeightInGram() - ingredient.getWeightInGram();

                        break;
                    }
                }

            }
            if (!hasIngredient) {
                System.out.println("There is no product needed in the refrigerator");
                return false;
            }
        }
        return true;
    }
}
