package homework_2.product;

public abstract class Product {
    public abstract String getName();

    public abstract double getCalories();

    public abstract double getFats();

    abstract double getСarbohydrates();


    abstract boolean isVegetarian();

}