package homework_2.product;

public class Spice extends Product {
    String name;
    double calories;
    double fats;
    double carbohydrates;

    public Spice(String name, double calories, double fats, double carbohydrates) {
        this.name = name;
        this.calories = calories;
        this.fats = fats;
        this.carbohydrates = carbohydrates;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getCalories() {
        return calories;
    }

    @Override
    public double getFats() {
        return fats;
    }

    @Override
    public double getСarbohydrates() {
        return carbohydrates;
    }

    @Override
    public boolean isVegetarian() {
        return true;
    }


}
