package homework_2;

import homework_2.product.*;
import homework_2.ingredients.Ingredient;
import homework_2.recipe.Recipe;
import homework_2.storage.Fridge;

import java.util.Arrays;

public class Chef {
    public void cook(Recipe recipe) {

        System.out.println(recipe.toString() + " has been cooked.");
    }


    public static void main(String[] args) {
        Chef chef = new Chef();
        Fridge fridge = null;
        try {
            fridge = new Fridge(Arrays.asList(
                    new Ingredient(new Meat("Beef", 250, 500, 200), 1000d),
                    new Ingredient(new Vegetable("Beet", 50, 20, 10), 1000d),
                    new Ingredient(new Vegetable("Bulb onion", 40, 30, 50), 1000d),
                    new Ingredient(new Spice("Pepper", 20, 3, 15), 50d),
                    new Ingredient(new Spice("Salt", 20, 0, 0), 200d),
                    new Ingredient(new Vegetable("Potato", 180, 47, 92), 1000d),
                    new Ingredient(new Vegetable("Tomato", 150, 83, 79), 1000d),
                    new Ingredient(new Vegetable("Cabbage", 60, 20, 10), 2000d),
                    new Ingredient(new Vegetable("Carrot", 63, 10, 40), 1000d),
                    new Ingredient(new Vegetable("Cucumber", 70, 47, 80), 1000d),
                    new Ingredient(new Meat("Chicken", 200, 300, 150), 1000d)
            ));
        } catch (FridgeException e) {
            e.printStackTrace();
        }

        Recipe recipe = null;
        try {
            recipe = new Recipe("Borscht", Arrays.asList(
                    new Ingredient(new Meat("Beef", 250, 500, 200), 500d),
                    new Ingredient(new Vegetable("Beet", 50, 20, 10), 50d),
                    new Ingredient(new Vegetable("Bulb onion", 40, 30, 50), 30d),
                    new Ingredient(new Spice("Pepper", 20, 3, 15), 5d),
                    new Ingredient(new Spice("Salt", 20, 0, 0), 5d),
                    new Ingredient(new Vegetable("Potato", 180, 47, 92), 200d),
                    new Ingredient(new Vegetable("Tomato", 150, 83, 79), 200d),
                    new Ingredient(new Vegetable("Cabbage", 60, 20, 10), 300d),
                    new Ingredient(new Vegetable("Carrot", 63, 10, 40), 60d)


            ));
        } catch (InvalidRecepieError invalidRecepieError) {
            invalidRecepieError.printStackTrace();
        }
        if (fridge.hasIngredients(recipe.getIngredientList())) {

            chef.cook(recipe);
            recipe.countСalories(recipe.getIngredientList());
            recipe.findIngredient(99, 180);

        }
        Recipe recipe2 = null;
        try {
            recipe2 = new Recipe("Chicken salad", Arrays.asList(
                    new Ingredient(new Meat("Chicken", 200, 300, 150), 300d),
                    new Ingredient(new Vegetable("Bulb onion", 40, 30, 50), 30d),
                    new Ingredient(new Spice("Pepper", 20, 3, 15), 5d),
                    new Ingredient(new Spice("Salt", 20, 0, 0), 5d),
                    new Ingredient(new Vegetable("Tomato", 150, 83, 79), 200d),
                    new Ingredient(new Vegetable("Cucumber", 70, 47, 80), 100d)


            ));
        } catch (InvalidRecepieError invalidRecepieError) {
            invalidRecepieError.printStackTrace();
        }
        if (fridge.hasIngredients(recipe2.getIngredientList())) {
            chef.cook(recipe2);
            recipe2.countСalories(recipe2.getIngredientList());
            recipe2.sortVegetable(recipe2.getIngredientList());
            System.out.println(recipe2.getIngredientList());

        }
        Recipe recipe3 = null;
        try {
            recipe3 = new Recipe("Empty recepie", null);
        } catch (InvalidRecepieError invalidRecepieError) {
            invalidRecepieError.printStackTrace();
        }
    }


}
