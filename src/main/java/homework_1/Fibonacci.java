package homework_1;

public class Fibonacci {
    public int fibonnachiCalculator(int number) {
        int[] fib = new int[number];
        fib[0] = 0;
        fib[1] = 1;
        int i = 2;
        while (i < number) {
            fib[i] = fib[i - 1] + fib[i - 2];
            i++;
        }

        return fib[i-1];
    }
}
