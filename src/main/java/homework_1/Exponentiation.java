package homework_1;

public class Exponentiation {
    public int exp(int number, int degree) {
        int result = 1;
        for (int i = 1; i <= degree; i++) {

            result *= number;

        }
        return result;
    }

}

