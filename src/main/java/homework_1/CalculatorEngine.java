package homework_1;

import java.util.Scanner;
import java.util.regex.Pattern;

public class CalculatorEngine {


    public void startCalculator() {

        Scanner sc = new Scanner(System.in);
        double number1 = 0;

        System.out.println("Enter first value");
        validator(number1, sc);
        number1 = sc.nextDouble();

        String operation = " ";
        System.out.println("Enter operation");
        validator(operation, sc);
        operation = sc.next();
        if ("F".equals(operation)) {
            Fibonacci fibonacci = new Fibonacci();
            int fibonacci_result = fibonacci.fibonnachiCalculator((int) number1);
            System.out.println(fibonacci_result);
            return;
        } else if ("!".equals(operation)) {
            Factorial factorial = new Factorial();
            int factorial_result = factorial.factorial((int) number1);
            System.out.println(factorial_result);

        } else {


            double number2 = 0;

            System.out.println("Enter second value");
            validator(number2, sc);
            number2 = sc.nextDouble();

            switch (operation) {
                case "+":
                    Addition ad = new Addition();
                    double addition_result = ad.add(number1, number2);
                    System.out.println(addition_result);
                    break;
                case "-":
                    Subtraction substr = new Subtraction();
                    int substr_result = substr.substr((int) number1, (int) number2);
                    System.out.println(substr_result);
                    break;
                case "*":
                    Multiplication mult = new Multiplication();
                    double multiplication_result = mult.mult(number1, number2);
                    System.out.println(multiplication_result);
                    break;
                case "^":
                    Exponentiation exp = new Exponentiation();
                    int exp_result = exp.exp((int) number1, (int) number2);
                    System.out.println(exp_result);
                    break;
                default:
                    System.out.println("Something went wrong. Try later.");
                    break;

            }
        }

        sc.close();
    }

    private void validator(double number, Scanner sc) {

        while (!sc.hasNextDouble()) {

            System.out.println("That's not a number.Try another value");
            sc.next();
        }

    }

    private void validator(String operation, Scanner sc) {
        Pattern pattern = Pattern.compile("[\\-\\*\\+\\^F\\!]");

        while (!sc.hasNext(pattern)) {

            System.out.println("You entered wrong value. Correct values are:'+', '-', '*', 'F'- calculate Fibonacci, '!'- calculate Factorial and '^'. ");
            sc.next();
        }

    }

}