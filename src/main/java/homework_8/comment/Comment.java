package homework_8.comment;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Comment {
    @SerializedName("id")
    private long comment_id;
    @SerializedName("post_id")
    private long post_id;
    @SerializedName("name")
    private String comment_name;
    private String email;
    @SerializedName("body")
    private String comment_body;

    public Comment(long post_id, String name, String email, String body) {
        this.comment_id = comment_id;
        this.post_id = post_id;
        this.comment_name = name;
        this.email = email;
        this.comment_body = body;
    }

    public long getComment_id() {
        return comment_id;
    }

    public long getPost_id() {
        return post_id;
    }

    public String getComment_name() {
        return comment_name;
    }

    public String getEmail() {
        return email;
    }

    public String getComment_body() {
        return comment_body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Comment)) return false;
        Comment comment = (Comment) o;
        return getComment_id() == comment.getComment_id() &&
                getPost_id() == comment.getPost_id() &&
                Objects.equals(getComment_name(), comment.getComment_name()) &&
                Objects.equals(getEmail(), comment.getEmail()) &&
                Objects.equals(getComment_body(), comment.getComment_body());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getComment_id(), getPost_id(), getComment_name(), getEmail(), getComment_body());
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + comment_id +
                ", post_id=" + post_id +
                ", name='" + comment_name + '\'' +
                ", email='" + email + '\'' +
                ", body='" + comment_body + '\'' +
                '}';
    }
}
