package homework_8.comment;

import com.google.gson.annotations.SerializedName;
import homework_8.CommonResponse;
import homework_8.Meta;


import java.util.Objects;

public class CommentResponse extends CommonResponse {

    @SerializedName("result")
    private Comment result;

    public CommentResponse(Meta meta, Comment result) {
        this.meta = meta;
        this.result = result;
    }

    public Comment getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommentResponse)) return false;
        CommentResponse that = (CommentResponse) o;
        return getMeta().equals(that.getMeta()) &&
                getResult().equals(that.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMeta(), getResult());
    }

    @Override
    public String toString() {
        return "CommentResponse{" +
                "meta=" + meta +
                ", result=" + result +
                '}';
    }
}
