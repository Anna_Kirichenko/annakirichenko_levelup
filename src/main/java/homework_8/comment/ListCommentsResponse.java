package homework_8.comment;

import com.google.gson.annotations.SerializedName;
import homework_8.CommonResponse;
import homework_8.Meta;

import java.util.List;
import java.util.Objects;

public class ListCommentsResponse extends CommonResponse {

    @SerializedName("result")
    private List<Comment> result;

    public ListCommentsResponse(Meta meta, List<Comment> result) {
        this.meta = meta;
        this.result = result;
    }


    public List<Comment> getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ListCommentsResponse)) return false;
        ListCommentsResponse that = (ListCommentsResponse) o;
        return Objects.equals(getMeta(), that.getMeta()) &&
                Objects.equals(getResult(), that.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMeta(), getResult());
    }

    @Override
    public String toString() {
        return "ListCommentsResponse{" +
                "meta=" + meta +
                ", result=" + result +
                '}';
    }
}
