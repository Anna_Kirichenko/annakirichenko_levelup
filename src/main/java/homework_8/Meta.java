package homework_8;

import java.util.Objects;

public class Meta {
    private boolean success;
    private int code;
    private String message;

    public Meta(boolean success, int code) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Meta)) return false;
        Meta meta = (Meta) o;
        return isSuccess() == meta.isSuccess() &&
                getCode() == meta.getCode() &&
                getMessage().equals(meta.getMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isSuccess(), getCode(), getMessage());
    }

    @Override
    public String toString() {
        return "Meta{" +
                "success=" + success +
                ", code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
