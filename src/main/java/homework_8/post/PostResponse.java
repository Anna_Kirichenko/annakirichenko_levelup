package homework_8.post;

import com.google.gson.annotations.SerializedName;
import homework_8.Meta;
import homework_8.CommonResponse;
import homework_8.comment.CommentResponse;
import homework_8.user.User;

import java.util.Objects;


public class PostResponse extends CommonResponse {
    @SerializedName("result")
    private Post result;

    public PostResponse(Meta meta, Post result) {
        this.meta = meta;
        this.result = result;
    }

    public Post getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PostResponse)) return false;
        PostResponse that = (PostResponse) o;
        return Objects.equals(getMeta(), that.getMeta()) &&
                Objects.equals(getResult(), that.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMeta(), getResult());
    }

    @Override
    public String toString() {
        return "PostResponse{" +
                "meta=" + meta +
                ", result=" + result +
                '}';
    }
}

