package homework_8.post;

import com.google.gson.annotations.SerializedName;
import homework_8.CommonResponse;
import homework_8.Meta;


import java.util.List;
import java.util.Objects;

public class ListPostResponse extends CommonResponse {

    @SerializedName("result")
    private List<Post> result;

    public ListPostResponse(Meta meta, List<Post> result) {
        this.meta = meta;
        this.result = result;
    }


    public List<Post> getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ListPostResponse)) return false;
        ListPostResponse that = (ListPostResponse) o;
        return getMeta().equals(that.getMeta()) &&
                getResult().equals(that.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMeta(), getResult());
    }

    @Override
    public String toString() {
        return "ListPostResponse{" +
                "meta=" + meta +
                ", result=" + result +
                '}';
    }
}
