package homework_8.post;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Post {
    @SerializedName("id")
    private long post_id;
    @SerializedName("user_id")
    private long user_id;
    @SerializedName("title")
    private String post_title;
    @SerializedName("body")
    private String post_body;

    public Post(long user_id, String title, String body) {
        this.user_id = user_id;
        this.post_title = title;
        this.post_body = body;
    }

    public long getPost_id() {
        return post_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public String getPost_title() {
        return post_title;
    }

    public String getPost_body() {
        return post_body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Post)) return false;
        Post post = (Post) o;
        return getPost_id() == post.getPost_id() &&
                getUser_id() == post.getUser_id() &&
                getPost_title().equals(post.getPost_title()) &&
                getPost_body().equals(post.getPost_body());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPost_id(), getUser_id(), getPost_title(), getPost_body());
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + post_id +
                ", user_id=" + user_id +
                ", title='" + post_title + '\'' +
                ", body='" + post_body + '\'' +
                '}';
    }
}
