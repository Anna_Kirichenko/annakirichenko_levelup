package homework_8;

import com.google.gson.annotations.SerializedName;


public abstract class CommonResponse {
    @SerializedName("_meta")
    protected Meta meta;


    protected Meta getMeta() {
        return meta;
    }


}
