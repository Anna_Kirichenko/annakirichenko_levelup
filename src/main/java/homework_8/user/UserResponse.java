package homework_8.user;

import com.google.gson.annotations.SerializedName;
import homework_8.CommonResponse;
import homework_8.Meta;

import java.util.Objects;

public class UserResponse extends CommonResponse {

    @SerializedName("result")
    private User result;

    public UserResponse(Meta meta, User result) {
        this.meta = meta;
        this.result = result;
    }


    public User getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserResponse)) return false;
        UserResponse that = (UserResponse) o;
        return Objects.equals(getMeta(), that.getMeta()) &&
                Objects.equals(getResult(), that.getResult());


    }

    @Override
    public int hashCode() {
        return Objects.hash(getMeta(), getResult());
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "meta=" + meta +
                ", result=" + result +
                '}';
    }
}
