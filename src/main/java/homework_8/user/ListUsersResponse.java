package homework_8.user;

import com.google.gson.annotations.SerializedName;
import homework_8.CommonResponse;
import homework_8.Meta;


import java.util.List;
import java.util.Objects;

public class ListUsersResponse extends CommonResponse {

    @SerializedName("result")
    private List<User> result;


    public ListUsersResponse(Meta meta, List<User> result) {
        this.meta = meta;
        this.result = result;
    }


    public List<User> getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ListUsersResponse)) return false;
        ListUsersResponse that = (ListUsersResponse) o;
        return getMeta().equals(that.getMeta()) &&
                getResult().equals(that.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMeta(), getResult());
    }

    @Override
    public String toString() {
        return "ListUsersResponse{" +
                "meta=" + meta +
                ", result=" + result +
                '}';
    }
}
