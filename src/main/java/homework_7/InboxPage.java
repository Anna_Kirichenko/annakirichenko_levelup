package homework_7;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;



public class InboxPage {

    //Email Page
    public static SelenideElement toField = $("[data-type='to'] input");
    public static SelenideElement subjectField = $("input[name='Subject']");
    public static SelenideElement textField = $("[role='textbox']");
    public static SelenideElement saveButton = $(By.xpath("//*[@title='Сохранить']"));
    public static SelenideElement sendButton = $(By.xpath("//*[@title='Отправить']"));
    public static SelenideElement closeButton = $(By.xpath("//*[@title='Закрыть']"));
    public static SelenideElement deleteButton = $(By.xpath("//*[@title='Удалить']"));

    //Draft Page
    public static SelenideElement draftFolder = $("[title^='Черновики']");
    public static SelenideElement draftEmpty = $(By.className("octopus__title"));
    public static ElementsCollection draftEmails = $$(By.className("llc__content"));
    public static ElementsCollection mailSubject = $$(By.xpath("//*[@class='llc__subject']"));
    public static ElementsCollection mailText = $$(By.xpath("//*[@class='ll-sp__normal']"));
    public static ElementsCollection mailSender = $$(By.xpath( "//*[@class='llc__item llc__item_correspondent']"));

    //Send mail Page
    public static SelenideElement send = $("[title^='Отправленные']");
    public static ElementsCollection sendEmails = $$(By.xpath("//*[@class='llc__item llc__item_correspondent']"));

    //Test folder elements
    public static SelenideElement testfolderClick = $("[title^='Test']");
    public static ElementsCollection testEmail = $$("div.llc__item_correspondent");
    public static ElementsCollection testMailText = $$(By.className("ll-sp__normal"));
    public static ElementsCollection testMailSubject = $$(By.className("llc__subject"));
    public static ElementsCollection testEmailSender = $$("div.llc__item_correspondent");

    //Trash folder elements
    public static SelenideElement trashButton = $(By.xpath( "//*[@href='/trash/']"));
    public static ElementsCollection trashMailSubject = $$(By.xpath("//*[@class='llc__subject']"));
    public static ElementsCollection trashMailText = $$(By.xpath("//*[@class='ll-sp__normal']"));
    public static ElementsCollection trashMailSender = $$(By.xpath( "//*[@class='ll-crpt']"));


    //Inbox Page

    public static ElementsCollection selfMails = $$(By.className("llc__content"));
    public static ElementsCollection inboxMailText = $$(By.className("ll-sp__normal"));
    public static ElementsCollection inboxMailSubject = $$(By.className("llc__subject"));
    public static ElementsCollection inboxMailSender = $$("div.llc__item_correspondent");
    public static SelenideElement selfEmailslink = $(By.xpath("//*[@class='mt-h-c__content']"));
   private static final SelenideElement createEmailButton = $(By.xpath("//*[@class='compose-button__txt']"));
    public static SelenideElement inBox = $("[class='nav__folder-name__txt']");
    public static SelenideElement inboxEmpty = $(By.className("octopus__title"));

    @Step("Создать письмо")
    public InboxPage pressCreateEmailButton() {
        createEmailButton.shouldBe(Condition.visible);
        createEmailButton.click();
        return this;
    }

}


