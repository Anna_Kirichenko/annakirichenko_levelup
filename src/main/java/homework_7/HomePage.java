package homework_7;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;


public class HomePage {
    private static final String URL = "https://mail.ru";
    @Step("Открывается домашняя страница 'https://mail.ru'")
    public HomePage open() {
        return Selenide.open(URL, HomePage.class);
    }

    private static final SelenideElement usernameTextField = $(By.id("mailbox:login"));
    private static final SelenideElement passwordTextField = $(By.id("mailbox:password"));
    private static final SelenideElement enterButton = $(By.id("mailbox:submit"));
    public static SelenideElement exitButton = $(By.id("PH_logoutLink"));

    @Step("Залогиниться")
    public void login(String username, String password) {
        usernameTextField.shouldBe(Condition.visible).sendKeys(username);
        enterButton.shouldBe(Condition.visible).click();
        passwordTextField.shouldBe(Condition.visible).sendKeys(password);
        enterButton.shouldBe(Condition.visible).click();
    }


}

