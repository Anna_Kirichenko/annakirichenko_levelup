package homewrok_6;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public abstract class PageObjectAbstractBase {
    protected WebDriver driver;
    protected WebDriverWait wait;

    protected PageObjectAbstractBase(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(driver, this);
    }

    protected void elementClick(WebElement element) {
        wait.until(elementToBeClickable(element)).click();
    }

    protected void sendKeysToElement(WebElement element, String text) {
        WebElement textFiled = wait.until(visibilityOf(element));
        textFiled.clear();
        textFiled.sendKeys(text);
    }


}
