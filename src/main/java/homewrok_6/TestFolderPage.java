package homewrok_6;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;


public class TestFolderPage extends PageObjectAbstractBase {

    @FindBy(css = "[title^='Test']")
    private WebElement send;
    @FindBy(css = "div.llc__item_correspondent")
    private List<WebElement> testEmail;

    @FindBy(className = "ll-sp__normal")
    private List<WebElement> mailText;

    @FindBy(className = "llc__subject")
    private List<WebElement> mailSubject;

    @FindBy(css = "div.llc__item_correspondent")
    private List<WebElement> mailSender;


    public TestFolderPage(WebDriver driver) {
        super(driver);

    }

    public void testFolderClick() {
        wait.until(elementToBeClickable(send)).click();
        wait.until(ExpectedConditions.titleContains("Test - Почта Mail.ru"));
    }

    public WebElement findEmailInTestFolder(int index) {
        WebElement indexOfMail = testEmail.get(index);
        return indexOfMail;
    }

    public String getMailSendAddress(int index) {
        String sender = mailSender.get(index).getText();
        return sender;
    }

    public String getMailText(int index) {
        String text = mailText.get(index).getText();
        return text;
    }

    public String getMailSubject(int index) {
        String subject = mailSubject.get(index).getText();
        return subject;
    }

}
