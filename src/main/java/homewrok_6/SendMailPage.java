package homewrok_6;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class SendMailPage extends PageObjectAbstractBase {

    @FindBy(css = "[title^='Отправленные']")
    private WebElement send;

    @FindBy(xpath = "//*[@class='llc__item llc__item_correspondent']")
    private List<WebElement> sendEmail;


    public SendMailPage(WebDriver driver) {
        super(driver);
    }

    public void navigatetoSendEmails() {
        wait.until(elementToBeClickable(send)).click();
        wait.until(ExpectedConditions.titleIs("Отправленные - Почта Mail.ru"));
    }


    public WebElement findEmailInSendMails(int index) {
        WebElement indexOfMail = sendEmail.get(index);
        return indexOfMail;
    }
}