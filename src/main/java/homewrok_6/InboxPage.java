package homewrok_6;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.testng.Assert.assertEquals;

public class InboxPage extends PageObjectAbstractBase {
    @FindBy(css = "[class='nav__folder-name__txt']")
    private WebElement inBox;

    @FindBy(xpath = "//*[@class='compose-button__txt']")
    private WebElement createEmailButton;

    @FindBy(xpath = "//*[@class='mt-h-c__content']")
    private WebElement selfEmailslink;

    @FindBy(className = "llc__content")
    private List <WebElement> selfmails;


    @FindBy(className = "ll-sp__normal")
    private List<WebElement> mailText;

    @FindBy(className = "llc__subject")
    private List<WebElement> mailSubject;

    @FindBy(css = "div.llc__item_correspondent")
    private List<WebElement> mailSender;

    public InboxPage(WebDriver driver) {
        super(driver);

    }

    public void validatePage() {
        wait.until(ExpectedConditions.visibilityOf(inBox));
        assertEquals(inBox.getText(), "Входящие");
    }

    public void pressCreateEmailButton() {
        elementClick(createEmailButton);
    }

    public void inboxClick() {
        wait.until(elementToBeClickable(inBox)).click();
        wait.until(ExpectedConditions.titleIs("Входящие - Почта Mail.ru"));
    }

    public WebElement findEmailInInbox(int index) {
        WebElement indexOfMail = selfmails.get(index);
        return indexOfMail;
    }

    public void clickSelfMails() {
        wait.until(elementToBeClickable(selfEmailslink)).click();
        wait.until(ExpectedConditions.titleContains("Входящие"));
    }
    public void openEmail(int index){
        WebElement indexOfMail = selfmails.get(index);
        indexOfMail.click();
    }

    public String getMailSendAddress(int index) {
        String sender = mailSender.get(index).getText();
        return sender;
    }

    public String getMailText(int index) {
        String text = mailText.get(index).getText();
        return text;
    }

    public String getMailSubject(int index) {
        String subject = mailSubject.get(index).getText();
        return subject;
    }
}
