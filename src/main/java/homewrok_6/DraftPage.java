package homewrok_6;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;


import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;


public class DraftPage extends PageObjectAbstractBase {

    @FindBy(css = "[title^='Черновики']")
    private WebElement drafts;
    @FindBy(className = "llc__content")
    private List<WebElement> draftEmails;

    @FindBy(xpath = "//*[@class='ll-sp__normal']")
    private List<WebElement> mailText;

    @FindBy(xpath = "//*[@class='llc__subject']")
    private List<WebElement> mailSubject;

    @FindBy(xpath = "//*[@class='llc__item llc__item_correspondent']")
    private List<WebElement> mailSender;

    @FindBy(className = "octopus__title")
    private WebElement draftEmpty;

    public DraftPage(WebDriver driver) {
        super(driver);

    }

    public void draftClick() {
        wait.until(elementToBeClickable(drafts)).click();
        wait.until(ExpectedConditions.titleIs("Черновики - Почта Mail.ru"));
    }

    public WebElement findEmailInDraft(int index) {
        WebElement indexOfMail = draftEmails.get(index);
        return indexOfMail;
    }

    public void openEmailbyIndex(int index) {
        WebElement indexOfMail = draftEmails.get(index);
        indexOfMail.click();
    }

    public String getMailSendAddress(int index) {
        String sender = mailSender.get(index).getText();
        return sender;
    }

    public String getMailText(int index) {
        String text = mailText.get(index).getText();
        return text;
    }

    public String getMailSubject(int index) {
        String subject = mailSubject.get(index).getText();
        return subject;
    }

    public String checkEmptyfolder() {
        WebElement draftfolder = wait.until(visibilityOf(draftEmpty));
        return draftEmpty.getText();
    }
}
