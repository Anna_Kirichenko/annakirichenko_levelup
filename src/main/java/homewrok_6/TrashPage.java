package homewrok_6;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class TrashPage extends PageObjectAbstractBase {
    @FindBy(xpath = "//*[@href='/trash/']")
    private WebElement trash;

    @FindBy(xpath = "//*[@class='ll-sp__normal']")
    private List<WebElement> mailText;

    @FindBy(xpath = "//*[@class='llc__subject']")
    private List<WebElement> mailSubject;

    @FindBy(xpath = "//*[@class='ll-crpt']")
    private List<WebElement> mailSender;

    public TrashPage(WebDriver driver) {
        super(driver);

    }

    public void trashClick() {
        wait.until(elementToBeClickable(trash)).click();
        wait.until(ExpectedConditions.titleIs("Корзина - Почта Mail.ru"));
    }

    public String getMailSendAddress(int index) {
        String sender = mailSender.get(index).getText();
        return sender;
    }
    public String getMailSubject(int index) {
        String subject = mailSubject.get(index).getText();
        return subject;
    }
    public String getMailText(int index) {
        String text = mailText.get(index).getText();
        return text;
    }



}
