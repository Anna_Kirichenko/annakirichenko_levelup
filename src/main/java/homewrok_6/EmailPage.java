package homewrok_6;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EmailPage extends PageObjectAbstractBase {

    @FindBy(css = "[data-type='to'] input")
    private WebElement to;
    @FindBy(css = "input[name='Subject']")
    private WebElement subject;
    @FindBy(css = "[role='textbox']")
    private WebElement emailText;
    @FindBy(xpath = "//*[@title='Сохранить']")
    private WebElement saveButton;
    @FindBy(xpath = "//*[@title='Отправить']")
    private WebElement sendButton;
    @FindBy(xpath = "//*[@title='Закрыть']")
    private WebElement closeButton;
    @FindBy(xpath = "//*[@title='Удалить']")
    private WebElement deleteButton;


    public EmailPage(WebDriver driver) {
        super(driver);

    }

    public void fillToField(String email) {
        sendKeysToElement(to, email);
    }

    public void fillSubjectField(String subj) {
        sendKeysToElement(subject, subj);
    }

    public void fillEmailText(String text) {
        sendKeysToElement(emailText, text);
    }

    public void pressSaveButton() {
        elementClick(saveButton);
    }

    public void pressCloseButton() {
        elementClick(closeButton);
    }

    public void pressSendButton() {
        elementClick(sendButton);
    }

    public void pressDeleteButton() {
        elementClick(deleteButton);
    }

}
