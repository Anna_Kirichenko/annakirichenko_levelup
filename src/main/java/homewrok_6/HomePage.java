package homewrok_6;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;


public class HomePage extends PageObjectAbstractBase {
    private static final String URL = "https://mail.ru";

    @FindBy(id = "mailbox:submit")
    private WebElement authorizationButton;

    @FindBy(id = "mailbox:login")
    private WebElement userNameTextField;

    @FindBy(id = "mailbox:password")
    private WebElement passwordTextField;

    @FindBy(id = "PH_logoutLink")
    private WebElement exitButton;


    public HomePage(WebDriver driver) {
        super(driver);

    }

    public HomePage open() {
        driver.get(URL);
        String title = driver.getTitle();
        Assert.assertEquals(title, "Mail.ru: почта, поиск в интернете, новости, игры");
        return this;

    }

    public void login(String username, String password) {
        sendKeysToElement(userNameTextField, username);
        elementClick(authorizationButton);
        sendKeysToElement(passwordTextField, password);
        elementClick(authorizationButton);
    }

    public void exitAccount() {
        elementClick(exitButton);
        wait.until(ExpectedConditions.titleIs("Mail.ru: почта, поиск в интернете, новости, игры"));
    }

}

